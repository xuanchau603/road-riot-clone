using System.Collections;
using System.Collections.Generic;
using Google.Protobuf;
using Google.Protobuf.Collections;
using Proto.GearInc;
using UnityEngine;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Person                   person      = new Person();
        Person.Types.PhoneNumber phoneNumber = new Person.Types.PhoneNumber();
        phoneNumber.Number = "123";
        phoneNumber.Type   = Person.Types.PhoneType.Work;

        person.Id     = 1;
        person.Name   = "LXC";
        person.Email  = "lxc@gmail.com";

        byte[] personData = person.ToByteArray();
        
        Debug.Log(personData);

        Person deserializedPerson = Person.Parser.ParseFrom(personData);
        
        Debug.Log(deserializedPerson);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
